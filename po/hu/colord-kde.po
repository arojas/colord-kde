# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Balázs Úr <urbalazs@gmail.com>, 2012, 2013.
# Kiszel Kristóf <kiszel.kristof@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-17 00:43+0000\n"
"PO-Revision-Date: 2017-03-04 14:40+0100\n"
"Last-Translator: Kiszel Kristóf <kiszel.kristof@gmail.com>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Kiszel Kristóf,Úr Balázs"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ulysses@kubuntu.org,urbalazs@gmail.com"

#: colord-kcm/ColordKCM.cpp:54
#, kde-format
msgid "Available Profiles"
msgstr "Elérhető profilok"

#: colord-kcm/ColordKCM.cpp:57 colord-kcm/ColordKCM.cpp:59
#, kde-format
msgid "Color settings"
msgstr "Színbeállítások"

#: colord-kcm/ColordKCM.cpp:61
#, kde-format
msgid "(C) 2012-2016 Daniel Nicoletti"
msgstr "© Daniel Nicoletti, 2012-2016."

#: colord-kcm/ColordKCM.cpp:62 icc-importer/main.cpp:69
#, kde-format
msgid "Port to kf5"
msgstr "Portolás KF5-re"

#: colord-kcm/ColordKCM.cpp:70
#, kde-format
msgid "From File..."
msgstr "Fájlból…"

#: colord-kcm/ColordKCM.cpp:202 colord-kcm/ColordKCM.cpp:372
#: icc-importer/main.cpp:139 icc-importer/main.cpp:142
#, kde-format
msgid "Importing Color Profile"
msgstr "Színprofil importálása"

#: colord-kcm/ColordKCM.cpp:260 colord-kcm/ColordKCM.cpp:265
#, kde-format
msgid "Are you sure you want to remove this profile?"
msgstr "Biztosan el akarja távolítani ezt a profilt?"

#. i18n: ectx: property (text), widget (QToolButton, removeProfileBt)
#: colord-kcm/ColordKCM.cpp:261 colord-kcm/ColordKCM.cpp:265
#: colord-kcm/ColordKCM.ui:33
#, kde-format
msgid "Remove Profile"
msgstr "Profil eltávolítása"

#: colord-kcm/ColordKCM.cpp:372
#, kde-format
msgid "Your profile did not match the device kind"
msgstr "A profil nem egyezik az eszköz fajtájával"

#: colord-kcm/ColordKCM.cpp:405
#, kde-format
msgid "You do not have any devices registered"
msgstr "Nincsen egyetlen regisztrált eszköze sem"

#: colord-kcm/ColordKCM.cpp:406
#, kde-format
msgid "Make sure colord module on kded is running"
msgstr "Győződjön meg arról, hogy a colord modul fut-e a kded-en"

#: colord-kcm/ColordKCM.cpp:409
#, kde-format
msgid "You do not have any profiles registered"
msgstr "Nincsen egyetlen regisztrált profilja sem"

#: colord-kcm/ColordKCM.cpp:410
#, kde-format
msgid "Add one by clicking Add Profile button"
msgstr "Adjon hozzá egyet a Profil hozzáadása gombra kattintva"

#. i18n: ectx: property (text), widget (QToolButton, profilesTb)
#: colord-kcm/ColordKCM.ui:52 colord-kcm/ProfileModel.cpp:225
#, kde-format
msgid "Profiles"
msgstr "Profilok"

#. i18n: ectx: property (text), widget (QToolButton, addProfileBt)
#: colord-kcm/ColordKCM.ui:161
#, kde-format
msgid "Add Profile"
msgstr "Profil hozzáadása"

#. i18n: ectx: property (text), widget (QToolButton, devicesTb)
#: colord-kcm/ColordKCM.ui:183 colord-kcm/DeviceModel.cpp:299
#, kde-format
msgid "Devices"
msgstr "Eszközök"

#: colord-kcm/Description.cpp:137
#, kde-format
msgid "Yes"
msgstr "Igen"

#: colord-kcm/Description.cpp:137
#, kde-format
msgid "None"
msgstr "Nincs"

#: colord-kcm/Description.cpp:166
#, kde-format
msgid "Named Colors"
msgstr "Nevesített színek"

#: colord-kcm/Description.cpp:174
#, kde-format
msgid "Metadata"
msgstr "Metaadat"

#: colord-kcm/Description.cpp:214
#, kde-format
msgctxt "device type"
msgid "Printer"
msgstr "Nyomtató"

#: colord-kcm/Description.cpp:216
#, kde-format
msgctxt "device type"
msgid "Display"
msgstr "Kijelző"

#: colord-kcm/Description.cpp:218
#, kde-format
msgctxt "device type"
msgid "Webcam"
msgstr "Webkamera"

#: colord-kcm/Description.cpp:220
#, kde-format
msgctxt "device type"
msgid "Scanner"
msgstr "Lapolvasó"

#: colord-kcm/Description.cpp:222
#, kde-format
msgctxt "device type"
msgid "Unknown"
msgstr "Ismeretlen"

#: colord-kcm/Description.cpp:229
#, kde-format
msgctxt "device scope"
msgid "User session"
msgstr "Felhasználói munkamenet"

#: colord-kcm/Description.cpp:231
#, kde-format
msgctxt "device scope"
msgid "Auto restore"
msgstr "Automatikus visszaállítás"

#: colord-kcm/Description.cpp:233
#, kde-format
msgctxt "device scope"
msgid "System wide"
msgstr "Rendszerszintű"

#: colord-kcm/Description.cpp:235
#, kde-format
msgctxt "device scope"
msgid "Unknown"
msgstr "Ismeretlen"

#: colord-kcm/Description.cpp:241 colord-kcm/Profile.cpp:202
#, kde-format
msgctxt "colorspace"
msgid "RGB"
msgstr "RGB"

#: colord-kcm/Description.cpp:243 colord-kcm/Profile.cpp:211
#, kde-format
msgctxt "colorspace"
msgid "CMYK"
msgstr "CMYK"

#: colord-kcm/Description.cpp:245 colord-kcm/Profile.cpp:205
#, kde-format
msgctxt "colorspace"
msgid "Gray"
msgstr "Szürke"

#: colord-kcm/Description.cpp:251
#, kde-format
msgid "This device has no profile assigned to it"
msgstr "Ehhez az eszközhöz nincs profil hozzárendelve"

#: colord-kcm/Description.cpp:368
#, kde-format
msgid "Create a color profile for the selected device"
msgstr "Színprofil létrehozása a kiválasztott eszközhöz"

#: colord-kcm/Description.cpp:378
#, kde-format
msgid "You need Gnome Color Management installed in order to calibrate devices"
msgstr ""
"A Gnome színkezelés telepítése szükséges az eszközök kalibrálásának érdekében"

#: colord-kcm/Description.cpp:381 colord-kcm/Description.cpp:390
#, kde-format
msgid ""
"The measuring instrument is not detected. Please check it is turned on and "
"correctly connected."
msgstr ""
"A mérőkészülék nem található. Ellenőrizze, hogy be van-e kapcsolva, és "
"megfelelően van-e csatlakoztatva."

#: colord-kcm/Description.cpp:408
#, kde-format
msgid "The measuring instrument does not support printer profiling."
msgstr "A mérőkészülék nem támogatja a nyomtatóprofilozást."

#: colord-kcm/Description.cpp:412
#, kde-format
msgid "The device type is not currently supported."
msgstr "Az eszköztípus jelenleg nem támogatott."

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: colord-kcm/Description.ui:21
#, kde-format
msgid "Information"
msgstr "Információ"

#. i18n: ectx: property (text), widget (QLabel, label)
#: colord-kcm/Description.ui:40
#, kde-format
msgid "Profile type:"
msgstr "Profil típus:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: colord-kcm/Description.ui:63
#, kde-format
msgid "Colorspace:"
msgstr "Színtér:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: colord-kcm/Description.ui:86
#, kde-format
msgid "Created:"
msgstr "Létrehozva:"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: colord-kcm/Description.ui:109
#, kde-format
msgid "Version:"
msgstr "Verzió:"

#. i18n: ectx: property (text), widget (QLabel, makeLabel)
#: colord-kcm/Description.ui:132
#, kde-format
msgid "Device Manufacturer:"
msgstr "Eszköz gyártó:"

#. i18n: ectx: property (text), widget (QLabel, modelLabel)
#: colord-kcm/Description.ui:158
#, kde-format
msgid "Device Model:"
msgstr "Eszközmodell:"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: colord-kcm/Description.ui:184
#, kde-format
msgid "Display correction:"
msgstr "Korrekció megjelenítése:"

#. i18n: ectx: property (text), widget (QLabel, label_11)
#: colord-kcm/Description.ui:207
#, kde-format
msgid "White Point:"
msgstr "Fehér pont:"

#. i18n: ectx: property (text), widget (QLabel, licenseLabel)
#: colord-kcm/Description.ui:230
#, kde-format
msgid "License:"
msgstr "Licenc:"

#. i18n: ectx: property (text), widget (QLabel, label_15)
#: colord-kcm/Description.ui:256
#, kde-format
msgid "File size:"
msgstr "Fájlméret:"

#. i18n: ectx: property (text), widget (QLabel, label_17)
#: colord-kcm/Description.ui:279
#, kde-format
msgid "Filename:"
msgstr "Fájlnév:"

#. i18n: ectx: property (text), widget (QPushButton, installSystemWideBt)
#: colord-kcm/Description.ui:302
#, kde-format
msgid "Install System Wide"
msgstr "Telepítés rendszerszinten"

#. i18n: ectx: property (text), widget (KTitleWidget, ktitlewidget)
#: colord-kcm/Description.ui:316
#, kde-format
msgid "device name"
msgstr "eszköznév"

#. i18n: ectx: property (comment), widget (KTitleWidget, ktitlewidget)
#: colord-kcm/Description.ui:319
#, kde-format
msgid "Printer"
msgstr "Nyomtató"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: colord-kcm/Description.ui:326
#, kde-format
msgid "Id:"
msgstr "Azonosító:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: colord-kcm/Description.ui:340
#, kde-format
msgid "Scope:"
msgstr "Hatáskör:"

#. i18n: ectx: property (text), widget (QLabel, label_10)
#: colord-kcm/Description.ui:354
#, kde-format
msgid "Mode:"
msgstr "Mód:"

#. i18n: ectx: property (text), widget (QLabel, label_13)
#: colord-kcm/Description.ui:368
#, kde-format
msgid "Current Profile:"
msgstr "Jelenlegi profil:"

#. i18n: ectx: property (text), widget (QPushButton, calibratePB)
#: colord-kcm/Description.ui:382
#, kde-format
msgid "Calibrate"
msgstr "Kalibrálás"

#. i18n: ectx: property (text), widget (KMessageWidget, msgWidget)
#: colord-kcm/Description.ui:400
#, kde-format
msgid "Print Test Page"
msgstr "Tesztoldal nyomtatása"

#. i18n: ectx: property (text), widget (KMessageWidget, msgWidget)
#: colord-kcm/Description.ui:405
#, kde-format
msgid "Clean Print Heads"
msgstr "Nyomtatófej-tisztítás"

#. i18n: ectx: property (text), widget (KMessageWidget, msgWidget)
#: colord-kcm/Description.ui:410
#, kde-format
msgid "Print Self Test Page"
msgstr "Önellenőrző oldal nyomtatása"

#. i18n: ectx: property (toolTip), widget (KMessageWidget, msgWidget)
#: colord-kcm/Description.ui:413
#, kde-format
msgid "Print Self-Test Page"
msgstr "Önellenőrző oldal nyomtatása"

#: colord-kcm/DeviceModel.cpp:229
#, kde-format
msgctxt "colorspace"
msgid "Default RGB"
msgstr "Alapértelmezett RGB"

#: colord-kcm/DeviceModel.cpp:231
#, kde-format
msgctxt "colorspace"
msgid "Default CMYK"
msgstr "Alapértelmezett CMYK"

#: colord-kcm/DeviceModel.cpp:233
#, kde-format
msgctxt "colorspace"
msgid "Default Gray"
msgstr "Alapértelmezett szürke"

#: colord-kcm/Profile.cpp:106
#, kde-format
msgid "file too small"
msgstr "a fájl túl kicsi"

#: colord-kcm/Profile.cpp:115
#, kde-format
msgid "not an ICC profile"
msgstr "nem ICC profil"

#: colord-kcm/Profile.cpp:187
#, kde-format
msgctxt "colorspace"
msgid "XYZ"
msgstr "XYZ"

#: colord-kcm/Profile.cpp:190
#, kde-format
msgctxt "colorspace"
msgid "LAB"
msgstr "LAB"

#: colord-kcm/Profile.cpp:193
#, kde-format
msgctxt "colorspace"
msgid "LUV"
msgstr "LUV"

#: colord-kcm/Profile.cpp:196
#, kde-format
msgctxt "colorspace"
msgid "YCbCr"
msgstr "YCbCr"

#: colord-kcm/Profile.cpp:199
#, kde-format
msgctxt "colorspace"
msgid "Yxy"
msgstr "Yxy"

#: colord-kcm/Profile.cpp:208
#, kde-format
msgctxt "colorspace"
msgid "HSV"
msgstr "HSV"

#: colord-kcm/Profile.cpp:214
#, kde-format
msgctxt "colorspace"
msgid "CMY"
msgstr "CMY"

#: colord-kcm/Profile.cpp:217
#, kde-format
msgctxt "colorspace"
msgid "Unknown"
msgstr "Ismeretlen"

#: colord-kcm/Profile.cpp:302
#, kde-format
msgid "Missing description"
msgstr "Hiányzó leírás"

#: colord-kcm/Profile.cpp:357
#, kde-format
msgctxt "profile kind"
msgid "Input device"
msgstr "Bemeneti eszköz"

#: colord-kcm/Profile.cpp:359
#, kde-format
msgctxt "profile kind"
msgid "Display device"
msgstr "Megjelenítő eszköz"

#: colord-kcm/Profile.cpp:361
#, kde-format
msgctxt "profile kind"
msgid "Output device"
msgstr "Kimeneti eszköz"

#: colord-kcm/Profile.cpp:363
#, kde-format
msgctxt "profile kind"
msgid "Devicelink"
msgstr "Eszközhivatkozás"

#: colord-kcm/Profile.cpp:365
#, kde-format
msgctxt "profile kind"
msgid "Colorspace conversion"
msgstr "Színtérkonverzió"

#: colord-kcm/Profile.cpp:367
#, kde-format
msgctxt "profile kind"
msgid "Abstract"
msgstr "Absztrakt"

#: colord-kcm/Profile.cpp:369
#, kde-format
msgctxt "profile kind"
msgid "Named color"
msgstr "Nevesített szín"

#: colord-kcm/Profile.cpp:371
#, kde-format
msgctxt "profile kind"
msgid "Unknown"
msgstr "Ismeretlen"

#: colord-kcm/Profile.cpp:518
#, kde-format
msgid "Default: %1"
msgstr "Alapértelmezett: %1"

#: colord-kcm/Profile.cpp:520
#, kde-format
msgid "Colorspace: %1"
msgstr "Színtér: %1"

#: colord-kcm/Profile.cpp:522
#, kde-format
msgid "Test profile: %1"
msgstr "Teszt profil: %1"

#: colord-kcm/Profile.cpp:524
#, kde-format
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: colord-kcm/ProfileMetaData.cpp:78
#, kde-format
msgid "Standard space"
msgstr "Szabványos tér"

#: colord-kcm/ProfileMetaData.cpp:81
#, kde-format
msgid "Display checksum"
msgstr "Ellenőrzőösszeg megjelenítése"

#: colord-kcm/ProfileMetaData.cpp:84
#, kde-format
msgid "Display model"
msgstr "Modell megjelenítése"

#: colord-kcm/ProfileMetaData.cpp:87
#, kde-format
msgid "Display serial number"
msgstr "Sorozatszám megjelenítése"

#: colord-kcm/ProfileMetaData.cpp:90
#, kde-format
msgid "Display PNPID"
msgstr "PNPID megjelenítése"

#: colord-kcm/ProfileMetaData.cpp:93
#, kde-format
msgid "Display vendor"
msgstr "Gyártó megjelenítése"

#: colord-kcm/ProfileMetaData.cpp:96
#, kde-format
msgid "File checksum"
msgstr "Fájl-ellenőrzőösszeg"

#: colord-kcm/ProfileMetaData.cpp:99
#, kde-format
msgid "Framework product"
msgstr "Keretrendszer termék"

#: colord-kcm/ProfileMetaData.cpp:102
#, kde-format
msgid "Framework program"
msgstr "Keretrendszer program"

#: colord-kcm/ProfileMetaData.cpp:105
#, kde-format
msgid "Framework version"
msgstr "Keretrendszer verzió"

#: colord-kcm/ProfileMetaData.cpp:108
#, kde-format
msgid "Data source type"
msgstr "Adatforrás típus"

#: colord-kcm/ProfileMetaData.cpp:111
#, kde-format
msgid "Mapping format"
msgstr "Leképezett formátum"

#: colord-kcm/ProfileMetaData.cpp:114
#, kde-format
msgid "Mapping qualifier"
msgstr "Leképezés minősítő"

#. i18n: ectx: property (text), widget (QLabel, label)
#: colord-kcm/ProfileMetaData.ui:39
#, kde-format
msgid ""
"Meta data is additional information stored in the profile for programs to "
"use."
msgstr ""
"A metaadat olyan további információ, amely a profilban van tárolva a "
"használt programhoz."

#. i18n: ectx: property (text), widget (QLabel, label)
#: colord-kcm/ProfileNamedColors.ui:17
#, kde-format
msgid "Named colors are specific colors defined in the profile"
msgstr "A nevesített színek a profilban meghatározott egyedi színek"

#: icc-importer/main.cpp:41
#, kde-format
msgid "Description: %1"
msgstr "Leírás: %1"

#: icc-importer/main.cpp:51
#, kde-format
msgid "Copyright: %1"
msgstr "Szerzői jog: %1"

#: icc-importer/main.cpp:62
#, kde-format
msgid "ICC Profile Installer"
msgstr "ICC profil telepítő"

#: icc-importer/main.cpp:64
#, kde-format
msgid "An application to install ICC profiles"
msgstr "Egy ICC profilokat telepítő alkalmazás"

#: icc-importer/main.cpp:66
#, kde-format
msgid "(C) 2008-2016 Daniel Nicoletti"
msgstr "© Daniel Nicoletti, 2012-2016."

#: icc-importer/main.cpp:77
#, kde-format
msgid "Do not prompt the user if he wants to install"
msgstr "Ne kérdezze meg a felhasználót, ha telepíteni szeretne"

#: icc-importer/main.cpp:78
#, kde-format
msgid "Color profile to install"
msgstr "A telepítendő színprofil"

#: icc-importer/main.cpp:93
#, kde-format
msgid "Failed to parse file: %1"
msgstr "Nem sikerült feldolgozni a fájlt: %1"

#: icc-importer/main.cpp:93
#, kde-format
msgid "Failed to open ICC profile"
msgstr "Nem sikerült megnyitni az ICC profilt"

#: icc-importer/main.cpp:102
#, kde-format
msgid "Color profile is already imported"
msgstr "A színprofil már importálva van"

#: icc-importer/main.cpp:103 icc-importer/main.cpp:113
#: icc-importer/main.cpp:125
#, kde-format
msgid "ICC Profile Importer"
msgstr "ICC profil importáló"

#: icc-importer/main.cpp:112
#, kde-format
msgid "ICC profile already installed system-wide"
msgstr "Az ICC profil már telepítve van rendszerszinten"

#: icc-importer/main.cpp:124
#, kde-format
msgid "Would you like to import the color profile?"
msgstr "Szeretné importálni a színprofilt?"

#: icc-importer/main.cpp:126
#, kde-format
msgid "import"
msgstr ""

#: icc-importer/main.cpp:139
#, kde-format
msgid "Failed to import color profile: color profile is already imported"
msgstr "A színprofil importálása nem sikerült: a színprofil már importálva van"

#: icc-importer/main.cpp:142
#, kde-format
msgid "Failed to import color profile: could not copy the file"
msgstr "A színprofil importálása nem sikerült: nem másolható a fájl"

#~ msgid "Daniel Nicoletti"
#~ msgstr "Daniel Nicoletti"

#, fuzzy
#~| msgid "Failed to import color profile"
#~ msgid "Failed to import color profile: file already exists"
#~ msgstr "A színprofil importálása nem sikerült"

#~ msgid "X Resize and Rotate extension version %1.%2"
#~ msgstr "X átméretezés és forgatás (XRANDR) kiterjesztés verzió %1.%2"
